
export class Cart {
  $key: string;
  item_name: string;
  item_image: string;
  item_price: number;
  item_vendeur: string;
  item_nomvendeur: string;
  item_phonevendeur: number;
  item_qty: number;
  name_img:string;
}
